import serial, sqlite3, datetime
arduino = serial.Serial("/dev/ttyACM0", 9600)
db = sqlite3.connect("weather.db")
cur = db.cursor()
cur.execute("""
CREATE TABLE IF NOT EXISTS temperature(
     t_date TEXT,
     t_val TEXT
)
""")
while arduino.is_open:
    data = {}
    data["date"] = datetime.datetime.now().strftime("%H:%M")
    for i in range(0,2):
        data["temperature"] = arduino.readline().decode("utf8").replace('\r\n', "")
        data["humidity"] = arduino.readline().decode("utf8").replace('\r\n', "")
    cur.execute("INSERT INTO temperature(t_date, t_val) VALUES(?,?)", (data["date"], data["temperature"]))
    db.commit()
db.close()