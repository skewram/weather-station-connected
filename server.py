import sqlite3, random, json
from http.server import BaseHTTPRequestHandler, HTTPServer
from datetime import datetime
class HttpServer(BaseHTTPRequestHandler):
    def do_GET(self):
        print("Execute SQL request")
        cur.execute("SELECT * FROM temperature")
        print("[||]====[||]=={ < RESULT > }==[||]====[||]")
        result = cur.fetchall()
        t = {}
        for i in range (0, len(result)):
            t[i] = {}
            t[i]["date"] = result[i][0]
            t[i]["temperture"] = result[i][1]
        print(t)
        self.send_response(200)
        self.send_header("Access-Control-Allow-Origin", "*")
        self.send_header("Content-type", "text/html")
        self.end_headers()
        self.wfile.write(bytes(json.dumps(t), "utf8"))
        return
print("Connecting to Database")
db = sqlite3.connect("weather.db")
cur = db.cursor()
print("Webserver communicate on port 8080")
httpd = HTTPServer(("", 8080), HttpServer)
httpd.serve_forever()
print("Closing connection to Database")
db.close()