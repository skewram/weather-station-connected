$(document).ready(function(){
  var humidity_canvas = $("#humidity_chart");
  new Chart(humidity_canvas, {
    type: 'doughnut',
    data: {
      datasets: [{
        data: [
          "12",
          "36",
          "62"
        ],
        backgroundColor: [
          "rgb(230, 126, 34)",
          "rgb(46, 204, 113)",
          "rgb(52, 152, 219)"
        ],
        borderColor: [
          "rgb(211, 84, 0)",
          "rgb(39, 174, 96)",
          "rgb(41, 128, 185)"
        ],
        borderWidth: 3
      }],
      labels: [
        "Sec",
        "Normal",
        "Humide"
      ]
    },
    legend: {
      labels: {
          fontColor: 'rgb(255, 255, 255)'
      }
    }
  });
  $("#athmos_chart").gaugeMeter();
});